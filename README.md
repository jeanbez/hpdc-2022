# Access Patterns and Performance Behaviors of Multilayer Supercomputer I/O Subsystems under Production Load

*Jean Luca Bez (1), Ahmad Maroof Karimi (2), Arnab K. Paul (2,4), Bing Xie (2), Suren Byna (1), Philip Carns (3), Sarp Oral (2), Feiyi Wang (2), Jesse Hanley (2)*
	
(1) Lawrence Berkeley National Laboratory, USA

(2) Oak Ridge National Laboratory, USA

(3) Argonne National Laboratory, USA

(4) Birla Institute of Technology & Science, India